from django.apps import AppConfig


class PesanmotivasiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pesanmotivasi'
