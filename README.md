# project

[![Test and Deploy][actions-badge]][commits-gh] [![pipeline status][pipeline-badge]][commits-gl] [![coverage report][coverage-badge]][commits-gl]

# Tugas Kelompok PBP F07

## Nama Anggota:
|Nama                           | NPM       |
|-------------------------------|-----------|
|Danang Widyorukmantiyoro A. S  |2006597203 |
|Farhan Hanif Saefuddin         |2006596642 |
|Josh Steven Lasiman            |2006596131 |
|Muhammad Dhafin Fauzan         |2006596421 |
|Muhammad Ilman Nafi'an         |2006597134 |
|Nurkhalif Rizqi Fatiha         |2006597071 |
|Rizky Juniastiar               |2006596043 |


## Link Heroku:
```
https://notedote.herokuapp.com/
```

## Deksripsi Aplikasi
Banyaknya masalah yang dialami oleh sebagian mahasiswa yang sulit dalam menentukan jadwal kegiatannya secara teratur karena berbagai kesibukan yang mereka alami seperti tugas mingguan,kuis, ataupun kegiatan lain di luar ranah akademis. Maka dari itu, untuk mengatasi masalah tersebut, kami berencana untuk membuat aplikasi penjadwalan kegiatan mahasiswa yang lebih khusus dalam ranah perkuliahan dan juga dilengkapi dengan berbagai fitur menarik serta interaktif.


## Daftar Modul:
1. Login & Sign up (Login Form, Sign up Form) - Alif
2. Profile Page - Settings (Form, Profile picture, Name) - Josh
3. To Do List  (Form, Add task, View task, Deadline)- Hanip
4. Notes (Form, Notes Add, View Notes) - Ilman
5. Personal Journal (Form, Journal Add, View Journal) - Dafin
6. Weekly Schedule (Form, Add Weekly Schedule, View Weekly Schedule) - Rizky
7. Motivation (Form, Add Motivation, View Quote) - Danang

## Peran pengguna (Persona)

### Student 
- Usia: 
    - (SMP sampai Kuliah)
- Jenis kelamin: 
    - Laki-laki 
    - Perempuan
- Preferensi (Likes/Dislikes):
    - Simple dan minimalis
- Goal: 
    - Melihat deadline to-do yang jelas
    - Mudah dalam menyusun to-do
- Pain:
    - Memiliki banyak tugas yang berdekatan deadline nya
    - Suka lupa kalau ada tugas apa saja

[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
[commits-gh]: https://gitlab.com/farhan.hanif/projek-f07/-/commits/master
[pipeline-badge]: https://gitlab.com/farhan.hanif/projek-f07/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/farhan.hanif/projek-f07/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/farhan.hanif/projek-f07/pipelines
